PKGS=price product
PKGPREF=cmd
PKG=app
OUT=$(PKG)
SERVICE=$(PKG)-service
CONTAINER=$(PKG)-container
NODE=$(PKG)-node
IMAGE=registry.gitlab.com/cmartel/gscal/$(CONTAINER)
PWD=$(shell pwd)


tests:
	@for pkg in $(PKGS); do \
		make -C $(PKGPREF)/$$pkg test PKG=$$pkg; \
	done

services:
	for pkg in $(PKGS); do \
		make -C $(PKGPREF)/$$pkg clean build PKG=$$pkg; \
	done

images:
	@for pkg in $(PKGS); do \
		make -C $(PKGPREF)/$$pkg docker-build PKG=$$pkg; \
	done

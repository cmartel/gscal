module github.com/abits/gscal/cmd/price

go 1.13

require (
	github.com/gin-gonic/gin v1.6.2
	github.com/google/go-github/v29 v29.0.3 // indirect
	github.com/gorilla/mux v1.7.4 // indirect
	github.com/stretchr/testify v1.5.1
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0 // indirect
)

package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

var port = "80"

func calculatePrice(kilowattHours float64, factor float64) (price float64) {
	return kilowattHours * factor
}

func getQueryParam(c *gin.Context) (kwh float64, factor float64) {
	_kwh := c.Query("kwh")
	_factor := c.Query("factor")
	kwh, _ = strconv.ParseFloat(_kwh, 64)
	factor, _ = strconv.ParseFloat(_factor, 64)
	return
}

func getPrice(c *gin.Context) {
	kwh, factor := getQueryParam(c)
	price := calculatePrice(kwh, factor)
	c.JSON(http.StatusOK, gin.H{"price": fmt.Sprintf("%.2f", price)})
}

func getStatus(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"status": "ok"})
}

func SetupRouter() *gin.Engine {
	router := gin.Default()
	router.GET("/price", getPrice)
	router.GET("/status", getStatus)
	return router
}

func main() {
	router := SetupRouter()
	log.Printf("Starting server on port: %s", port)
	router.Run(fmt.Sprintf(":%s", port))
}

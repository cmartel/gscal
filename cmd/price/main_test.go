package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func performRequest(r http.Handler, method, path string, params map[string]string) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, nil)
	q := req.URL.Query()
	for k, v := range params {
		q.Add(k, v)
	}
	req.URL.RawQuery = q.Encode()
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestGetPrice(t *testing.T) {
	router := SetupRouter()
	var w *httptest.ResponseRecorder
	var response map[string]string
	var expect gin.H
	var params map[string]string

	params = map[string]string{
		"kwh":    "10000",
		"factor": "10.6",
	}
	expect = gin.H{
		"price": "106000.00",
	}
	w = performRequest(router, "GET", "/price", params)
	assert.Equal(t, http.StatusOK, w.Code)

	err := json.Unmarshal([]byte(w.Body.String()), &response)
	value, exists := response["price"]
	assert.Nil(t, err)
	assert.True(t, exists)
	assert.Equal(t, expect["price"], value)

	params = map[string]string{
		"kwh": "10000",
	}
	expect = gin.H{
		"price": "0.00",
	}
	w = performRequest(router, "GET", "/price", params)
	assert.Equal(t, http.StatusOK, w.Code)

	err = json.Unmarshal([]byte(w.Body.String()), &response)
	value, exists = response["price"]
	assert.Nil(t, err)
	assert.True(t, exists)
	assert.Equal(t, expect["price"], value)

	params = map[string]string{
		"factor": "10.6",
	}
	expect = gin.H{
		"price": "0.00",
	}
	w = performRequest(router, "GET", "/price", params)
	assert.Equal(t, http.StatusOK, w.Code)

	err = json.Unmarshal([]byte(w.Body.String()), &response)
	value, exists = response["price"]
	assert.Nil(t, err)
	assert.True(t, exists)
	assert.Equal(t, expect["price"], value)

	params = map[string]string{}
	expect = gin.H{
		"price": "0.00",
	}
	w = performRequest(router, "GET", "/price", params)
	assert.Equal(t, http.StatusOK, w.Code)

	err = json.Unmarshal([]byte(w.Body.String()), &response)
	value, exists = response["price"]
	assert.Nil(t, err)
	assert.True(t, exists)
	assert.Equal(t, expect["price"], value)

	params = map[string]string{
		"kwh":    "abcde",
		"factor": "10.6",
	}
	expect = gin.H{
		"price": "0.00",
	}
	w = performRequest(router, "GET", "/price", params)
	assert.Equal(t, http.StatusOK, w.Code)

	err = json.Unmarshal([]byte(w.Body.String()), &response)
	value, exists = response["price"]
	assert.Nil(t, err)
	assert.True(t, exists)
	assert.Equal(t, expect["price"], value)
}

func TestGetStatus(t *testing.T) {
	router := SetupRouter()
	var w *httptest.ResponseRecorder
	var response map[string]string
	var expect gin.H
	var params map[string]string

	expect = gin.H{
		"status": "ok",
	}

	w = performRequest(router, "GET", "/status", params)
	assert.Equal(t, http.StatusOK, w.Code)

	err := json.Unmarshal([]byte(w.Body.String()), &response)
	value, exists := response["status"]
	assert.Nil(t, err)
	assert.True(t, exists)
	assert.Equal(t, expect["status"], value)
}

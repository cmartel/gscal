package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

var port = "81"

func getQueryParam(c *gin.Context) (id string) {
	id = c.Query("id")
	return
}

func getProduct(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"product": "ok"})
}

func getStatus(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"status": "ok"})
}

func SetupRouter() *gin.Engine {
	router := gin.Default()
	router.GET("/status", getStatus)
	return router
}

func main() {
	router := SetupRouter()
	log.Printf("Starting server on port: %s", port)
	router.Run(fmt.Sprintf(":%s", port))
}
